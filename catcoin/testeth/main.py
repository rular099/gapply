from selenium import webdriver
from PIL import Image
import muggle_ocr
from ecdsa import SigningKey, SECP256k1
import sha3
import time
import socket

hostname = socket.gethostname()
fireFoxOptions = webdriver.FirefoxOptions()
fireFoxOptions.set_headless()
def checksum_encode(addr_str): # Takes a hex (string) address as input
    keccak = sha3.keccak_256()
    out = ''
    addr = addr_str.lower().replace('0x', '')
    keccak.update(addr.encode('ascii'))
    hash_addr = keccak.hexdigest()
    for i, c in enumerate(addr):
        if int(hash_addr[i], 16) >= 8:
            out += c.upper()
        else:
            out += c
    return '0x' + out

def gen_addr():
    keccak = sha3.keccak_256()
    priv = SigningKey.generate(curve=SECP256k1)
    pub = priv.get_verifying_key().to_string()
    keccak.update(pub)
    address = keccak.hexdigest()[24:]
    privatekey = "Private key:"+priv.to_string().hex()+'\n'
    pubkey = "Public key: "+ pub.hex() + '\n'
    addr = "Address: "+ checksum_encode(address) + '\n'
    with open('/home/ubuntu/'+hostname+'_done.log','a') as f:
        f.write(privatekey)
        f.write(pubkey)
        f.write(addr)
        f.write('\n')
    return checksum_encode(address)

def gen_new_addr():
    keccak = sha3.keccak_256()
    priv = SigningKey.generate(curve=SECP256k1)
    pub = priv.get_verifying_key().to_string()
    keccak.update(pub)
    address = keccak.hexdigest()[24:]
    privatekey = "Private key:"+priv.to_string().hex()+'\n'
    pubkey = "Public key: "+ pub.hex() + '\n'
    addr = "Address: "+ checksum_encode(address) + '\n'
    with open('/home/ubuntu/'+hostname+'_mainadd.log','a') as f:
        f.write(privatekey)
        f.write(pubkey)
        f.write(addr)
        f.write('\n')
    return checksum_encode(address)

def get_captcha(sdk):
    im = Image.open('/home/ubuntu/test_screenshot.png')
    left = 123
    top = 420
    right = 221
    bottom = 458
    im1 = im.crop((left,top,right,bottom))
    im1.save('/home/ubuntu/im1.png')
    with open("/home/ubuntu/im1.png", "rb") as f:
        b = f.read()
        text = sdk.predict(image_bytes=b)
    return text

def scrape(sdk,url):
    addr = gen_addr()
    browser = webdriver.Firefox(options=fireFoxOptions)
    browser.get(url)
    browser.save_screenshot('/home/ubuntu/test_screenshot.png')
    captcha = get_captcha(sdk)
    print(captcha)
    forms = browser.find_elements_by_class_name('form-control')
    forms[0].send_keys(addr)
    forms[1].send_keys(captcha)
    buttons = browser.find_elements_by_tag_name('button')
    buttons[1].click()
    browser.close()
    browser.quit()

def scrape_new(sdk):
    addr = gen_new_addr()
    browser = webdriver.Firefox(options=fireFoxOptions)
    browser.get('https://catcoin.link')
    browser.save_screenshot('/home/ubuntu/test_screenshot.png')
    captcha = get_captcha(sdk)
    print(captcha)
    forms = browser.find_elements_by_class_name('form-control')
    forms[0].send_keys(addr)
    forms[1].send_keys(captcha)
    buttons = browser.find_elements_by_tag_name('button')
    buttons[1].click()
    time.sleep(5)
    elem = browser.find_element_by_id('myurl')
    ref_url = elem.get_property('value')
    browser.close()
    browser.quit()
    return ref_url

if __name__ == '__main__' :
    sdk = muggle_ocr.SDK(model_type=muggle_ocr.ModelType.Captcha)
    ref_url = scrape_new(sdk)
    while True:
        scrape(sdk,ref_url)
        time.sleep(31)
