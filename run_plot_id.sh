#!/bin/bash
cd /home/ubuntu/chia-blockchain
. ./activate
chia plots create -k 32 -b 32000 -r 2 -n 5 -f 967cfcaf080464ea7d897b4512902f352e73a2401332c38b08725c0d43bdee15efe41ee65027de404c591f29c6e2319b -p 8bb49629ab7cc17708a9e56fbf765bbd1eaa8fe5531ee4b284a239957fa692d28f335212670cb84904776671afb51834 -a 360618117 -t /mnt/disks/plot$1/plot_tmp -d /mnt/disks/plot$1/plot_final
