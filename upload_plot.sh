for j in {1..4}
do
  cd /mnt/disks/plot$j/plot_final
  for i in *.plot
  do
    gclone copy $i gc:{0AOGGkmLY7TfrUk9PVA} --drive-server-side-across-configs -Pv && rm $i
  done
done
